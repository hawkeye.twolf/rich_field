<?php

namespace Drupal\rich_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\rich_field\Plugin\Field\RichFieldTrait;
use Drupal\Core\Field\FieldItemInterface;

/**
 * Plugin implementation of the 'rich_field' field type.
 *
 * @todo
 * - UI for selecting subfields.
 * - Now that we're extending FieldItemBase and not implementing the interface
 *   directly, do we need to call the parent methods in some cases?
 *
 * @FieldType(
 *   id = "rich_field",
 *   label = @Translation("Rich Field"),
 *   description = @Translation("A field made of sub-fields."),
 *   default_widget = "rich_field",
 *   default_formatter = "rich_field",
 * )
 */
class RichItem extends FieldItemBase {

  use RichFieldTrait;

  /**
   * The subfield item instances.
   *
   * @var \Drupal\Core\Field\FieldItemInterface[]
   */
  protected $subfields = [];

  /**
   * Provides the field type manager service.
   *
   * @return \Drupal\Core\Field\FieldTypePluginManagerInterface
   *   The field type manager service.
   */
  protected static function fieldPluginManager() {
    static $manager;
    return $manager ?? $manager = \Drupal::service('plugin.manager.field.field_type');
  }

  /**
   * {@inheritdoc}
   */
  protected static function getSubfieldTypes() {
    return [
      'foo' => 'string',
      'bar' => 'telephone',
      'bat' => 'link',
      'baz' => 'entity_reference',
      'whiz' => 'text',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);

    // Create subfield instances.
    foreach (static::getSubfieldTypes() as $subname => $subtype) {
      $subfield_definition = static::getSubfieldDefinition($definition
        ->getFieldDefinition(), $subname);
      $this->subfields[$subname] = static::fieldPluginManager()
        ->createInstance($subtype, [
          'type' => $subtype,
          'name' => static::getSubfieldName($name, $subname),
          // Some field types assume that their parent is the entity.
          'parent' => $parent,
          'settings' => $subfield_definition
            ->getSettings(),
          'field_definition' => $subfield_definition,
        ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return $this
      ->getFieldDefinition()
      ->getSettings() + $this
      ->instanceUnion(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   *
   * @todo Implement Rich Field variation.
   */
  protected function getSetting($setting_name) {
    return parent::getSetting($setting_name);
  }

  /**
   * {@inheritdoc}
   *
   * @todo Implement Rich Field variation?
   */
  protected function writePropertyValue($property_name, $value) {
    return parent::writePropertyValue($property_name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    foreach (static::getSubfieldClasses() as $name => $class) {
      $subfield_definition = static::getSubfieldDefinition($field_definition, $name);
      $subproperty_definitions = $class::propertyDefinitions($subfield_definition);
      foreach ($subproperty_definitions as $key => $property_definition) {
        $altered_key = static::alterKey($key, $name);
        // @todo Support requiring subfields (ie, some required, some optional).
        $property_definitions[$altered_key] = $property_definition
          ->setRequired(FALSE);
      }
    }
    return $property_definitions ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [];
    foreach (static::getSubfieldTypes() as $name => $type) {
      $subschema = static::getSubfieldDefinition($field_definition, $name)
        ->getSchema();
      // See FieldItemInterface for details about schema info array structure.
      // @see \Drupal\Core\Field\FieldItemInterface ::schema()
      foreach ($subschema as &$schema_info) {
        static::alterKeysByReference($schema_info, $name);
      }
      // Although we altered the schema info keys above, we must also alter the
      // array values of the indexes, since they reference columns.
      foreach ($subschema['indexes'] as &$index) {
        foreach ($index as &$column) {
          // Some index definitions use an array format ([column, int]).
          if (is_array($column)) {
            static::alterKeyByReference($column[0], $name);
          }
          else {
            static::alterKeyByReference($column, $name);
          }
        }
      }
      $schema = array_merge_recursive($schema, $subschema);
    }
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function __get($name) {

    // Handle the "whole subfield" case.
    if (isset($this->subfields[$name])) {
      return $this->subfields[$name];
    }
    // Provide a subfield value.
    foreach ($this->subfields as $subfield_name => $subfield) {
      $normalized_name = static::normalizeKey($name, $subfield_name);
      if ($normalized_name !== FALSE) {
        return $subfield->$normalized_name;
      }
    }
    return parent::__get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function __set($name, $value) {

    // Is an entire subfield being set?
    if (isset($this->subfields[$name])) {
      // Handle the case where $value is an actual subfield item instance.
      if ($value instanceof FieldItemInterface) {
        $value = $value
          ->getValue();
      }
      // Set the values array on the subfield.
      $this->subfields[$name]
        ->setValue($value);
      return;
    }
    // Set the individual property of a specific subfield.
    else {
      foreach ($this->subfields as $subfield_name => $subfield) {
        $normalized_name = static::normalizeKey($name, $subfield_name);
        if ($normalized_name !== FALSE) {
          $subfield->$normalized_name = $value;
          return;
        }
      }
    }
    return parent::__set($name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function __isset($name) {

    // Handle the "whole subfield" case.
    if (isset($this->subfields[$name])) {
      return TRUE;
    }
    // Check a subfield value.
    foreach ($this->subfields as $subfield_name => $subfield) {
      $normalized_name = static::normalizeKey($name, $subfield_name);
      if ($normalized_name !== FALSE) {
        return isset($subfield->$normalized_name);
      }
    }
    return parent::__isset($name);
  }

  /**
   * {@inheritdoc}
   */
  public function __unset($name) {

    // Handle the "whole subfield" case.
    if (isset($this->subfields[$name])) {
      $this->subfields[$name]
        ->setValue([]);
      return;
    }
    // Unset a subfield value.
    foreach ($this->subfields as $subfield_name => $subfield) {
      $normalized_name = static::normalizeKey($name, $subfield_name);
      if ($normalized_name !== FALSE) {
        unset($subfield->$normalized_name);
        return;
      }
    }
    return parent::__unset($name);
  }

  /**
   * {@inheritdoc}
   */
  public function __clone() {
    foreach ($this->subfields as &$subfield) {
      $subfield = clone $subfield;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function view($display_options = []) {
    return array_map(function ($subfield) use ($display_options) {
      return $subfield
        ->view($display_options);
    }, $this->subfields);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    return $this
      ->call(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $post_save = FALSE;
    foreach ($this->subfields as $subfield) {
      // If any of the subfields' post save methods return TRUE, return TRUE for
      // the overall field.
      $post_save = $subfield
        ->postSave($update) || $post_save;
    }
    return $post_save;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    return $this
      ->call(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision() {
    return $this
      ->call(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return static::unionWithFieldDefinition(__FUNCTION__, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return static::union(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return static::union(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public static function storageSettingsToConfigData(array $settings) {
    return static::unionWithSettings(__FUNCTION__, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function storageSettingsFromConfigData(array $settings) {
    return static::unionWithSettings(__FUNCTION__, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldSettingsToConfigData(array $settings) {
    return static::unionWithSettings(__FUNCTION__, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldSettingsFromConfigData(array $settings) {
    return static::unionWithSettings(__FUNCTION__, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return $this
      ->unionWithForm(__FUNCTION__, ['settings'], $form, $form_state, $has_data);
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    return $this
      ->unionWithForm(__FUNCTION__, ['settings'], $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateDependencies(FieldDefinitionInterface $field_definition) {
    return static::unionWithFieldDefinition(__FUNCTION__, $field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateStorageDependencies(FieldStorageDefinitionInterface $field_storage_definition) {
    return static::unionWithFieldDefinition(__FUNCTION__, $field_storage_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function onDependencyRemoval(FieldDefinitionInterface $field_definition, array $dependencies) {
    $changed = FALSE;
    foreach (static::getSubfieldClasses() as $name => $class) {
      // @todo Do we need to check whether the dependency belongs to the
      // subfield before calling onDependencyRemoval()?
      $subfield_definition = static::getSubfieldDefinition($field_definition, $name);
      $changed = $class::onDependencyRemoval($subfield_definition, $dependencies) || $changed;
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {

    // Get an entire subfield?
    if (isset($this->subfields[$property_name])) {
      return $this->subfields[$property_name];
    }
    // Get a subfield value.
    foreach ($this->subfields as $subfield_name => $subfield) {
      $normalized_name = static::normalizeKey($property_name, $subfield_name);
      if ($normalized_name !== FALSE) {
        return $subfield
          ->get($normalized_name);
      }
    }
    return parent::get($property_name);
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value, $notify = TRUE) {

    // Is an entire subfield being set?
    if (isset($this->subfields[$property_name])) {
      // Handle the case where $value is an actual subfield item instance.
      if ($value instanceof FieldItemInterface) {
        $value = $value
          ->getValue();
      }
      // Set the values array on the subfield.
      $this->subfields[$property_name]
        ->setValue($value, $notify);
      return $this;
    }
    // Set the individual property of a specific subfield.
    foreach ($this->subfields as $subfield_name => $subfield) {
      $normalized_name = static::normalizeKey($property_name, $subfield_name);
      if ($normalized_name !== FALSE) {
        $subfield
          ->set($normalized_name, $value, $notify);
        return $this;
      }
    }
    return parent::set($property_name, $value, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties($include_computed = FALSE) {
    return $this
      ->instanceUnion(__FUNCTION__, $include_computed);
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return $this
      ->instanceUnion(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    foreach ($this->subfields as $subfield) {
      // If any one subfield is not empty, then the whole field is not empty.
      if (!$subfield
        ->isEmpty()) {
        return FALSE;
      }
    }
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this
      ->instanceUnion(__FUNCTION__);
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {

    // Prevent duplicate notifications by setting notify to FALSE when setting
    // values and then notifying just once at the end.
    //
    // Set any non-subfield values.
    $parent_values = array_filter($values, function ($key) {
      return !array_key_exists($key, $this->subfields) && strpos($key, static::getPrefix()) !== 0;
    }, ARRAY_FILTER_USE_KEY);
    parent::setValue($parent_values, FALSE);

    // The parent class (FieldItemBase) supports single values by assuming they
    // belong to the field item's first property, but that doesn't work here.
    if (!is_array($values)) {
      $values = parent::getValue();
    }

    // Set subfield values. When setting subfield values, include any parent
    // values that don't belong to any one specific subfield.
    foreach ($this->subfields as $name => $subfield) {
      // @todo Consider using RichWidget::massageFormValues() to normalize the
      // form state values rather than detecting and handling it here.
      //
      // Handle data (from the field widget) in format:
      // @code
      // [
      //   'subfield_name' => [
      //     'subfield_key' => 'subfield_value',
      //   ]
      // ]
      // @endcode
      if (isset($values[$name])) {
        $subfield
          ->setValue($values[$name], FALSE);
        foreach ($parent_values as $parent_value_key => $parent_value) {
          $subfield
            ->set($parent_value_key, $parent_value, FALSE);
        }
      }
      // Handle data (from field storage) in the format:
      // @code
      // [
      //   'rich_field__subfield_name__subfield_key' => 'subfield_value',
      // ]
      // @endcode
      else {
        $subfield
          ->setValue(static::normalizeKeys($values, $name), FALSE);
      }
    }

    // Notify.
    $this
      ->onChange($this->name, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getString() {
    $string = parent::getString();
    foreach ($this->subfields as $subfield) {
      $string .= $subfield
        ->getString();
    }
    return $string;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Errors in constraint validation on saving field values.
   */
  public function getConstraints() {
    // @todo Determine whether we need to aggregate constraints. If this method
    // only ever gets called so its constraints can be passed to validate(),
    // then we don't need to worry about exposing any constraints here, since
    // the subfields validate themselves. But if the constraints get used for
    // things other than the validate() method, we'll need to reach into all the
    // subfield constraints and alter the keys of the constrained properties.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $violations = NULL;
    foreach ($this->subfields as $subfield) {
      $subfield_violations = $subfield
        ->validate();
      $violations = $violations === NULL ? $subfield_violations : $violations
        ->addAll($subfield_violations);
    }
    return $violations ?? \Drupal::typedDataManager()
      ->getValidator()
      ->validate($this);
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    $return = $this
      ->call(__FUNCTION__, FALSE);
    $this
      ->onChange($this->name, $notify);
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function setContext($name = NULL, TraversableTypedDataInterface $parent = NULL) {
    foreach ($this->subfields as $subname => $subfield) {
      $subfield
        ->setContext(static::getSubfieldName($name, $subname), $parent);
    }
    return parent::setContext($name, $parent);
  }

}
